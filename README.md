# Quick and dirty script to control the jerky dryer

Needs Brennstuhl-remote-switches and a nifty little utility called *send* on a Raspberry Pi.
It takes an integer as input and takes uses that as hours of drying the beef.

This little program maybe ain't pretty but it get's the job done.
Time was of the essence programming this and it does in fact work quite well.