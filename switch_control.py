#!/usr/bin/python3

from subprocess import call  # For calling bash commands


def control_switch(status):
    if status:
        # 000101 is the homecode. 3 is the number of the switch. Last digit is on or off.
        call("sudo /home/pi/bin/send 00101 3 1", shell=True)
        print("Ok, let's go! Let the jerky begin!")
    else:
        call("sudo /home/pi/bin/send 00101 3 0", shell=True)
        print("Ding! Your jerky is done!")
