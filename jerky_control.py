#!/usr/bin/python3

import sys
import time

import switch_control

# Set variables for jerky time
# It gets an argument from the cmdline in hrs. That'll be the time until the jerky is done
time_in_hour = float(sys.argv[1])


def print_time():
    current_time = time.localtime()
    print(
        "The current time is: " + current_time + ". You've set the jerky timer to: " + time_in_hour + "hours, which is " + round(
            time_in_hour / 60) + " minutes.")


# Start the jerky time
wait_time = (time_in_hour * 3600)
# print_time()
switch_control.control_switch(True)
time.sleep(wait_time)
switch_control.control_switch(False)
print(time.localtime())

# TODO:
# * Report leftover time every 30 minutes
# * Mail user when jerky is done
